package com.huike.domain.report.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;


@AllArgsConstructor
@Data
@Builder
public class OperationalDataVO {

    private String date;
    private Integer newClue;
    private Integer newBusiness;
    private Integer newContract;
    private Double sale;
}

package com.huike.domain.report.vo;

import lombok.Data;

@Data
public class DataStatistic {

    private String edate;

    private Integer num;

    private Double amount;
}

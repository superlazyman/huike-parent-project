package com.huike.test;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;
import java.io.File;

@SpringBootTest
public class SendMailTest {

    @Autowired
    private JavaMailSender javaMailSender;

    @Test
    public void testSendSimpleMail(){
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom("1443607071@qq.com");
        simpleMailMessage.setCc("1541980859@qq.com"); //抄送
        simpleMailMessage.setBcc("1541980859@qq.com"); //密送
        simpleMailMessage.setSubject("Mail测试邮箱");
        simpleMailMessage.setText("这是一封测试邮件 .....");

        javaMailSender.send(simpleMailMessage);
    }



    @Test
    public void testSendMail() throws Exception {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
        mimeMessageHelper.setFrom("1443607071@qq.com");
        mimeMessageHelper.setTo("1541980859@qq.com");
        mimeMessageHelper.setCc("1541980859@qq.com"); //抄送
        mimeMessageHelper.setBcc("1541980859@qq.com"); //密送
        mimeMessageHelper.setSubject("Mail测试邮箱");

        mimeMessageHelper.setText("<h1>这是一封测试邮件 .....<h1>" , true);
        mimeMessageHelper.addAttachment("1.jpg", new File("D:\\photo\\background1.jpg"));
        mimeMessageHelper.addAttachment("2.pdf", new File("D:\\photo\\未命名1_加水印.pdf"));
        javaMailSender.send(mimeMessage);
    }

}

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<style>
    p{
     text-align: center;
    }
    th{
        background-color: #FFC268;
    }
    td{
        background-color: #E3E3E3;
        text-align: center;
    }
    table{
        width: 60%;
        margin: auto;
    }
</style>
<body>

<p>你好，超级管理员，以下是本月公司运营是数据，请及时查找，数据有任何问题，请联系开发人员。</p>
<table border="1px" cellspacing="0" width="400px">
    <tr>
        <th>日期</th>
        <th>线索新增</th>
        <th>商机新增</th>
        <th>合同新增</th>
        <th>销售额</th>
    </tr>

    <#list operationalDataVOList as opData>
        <tr>
            <td>${opData.date}</td>
            <td>${opData.newClue}</td>
            <td>${opData.newBusiness}</td>
            <td>${opData.newContract}</td>
            <td>${opData.sale}</td>
        </tr>
    </#list>
</table>
</body>
</html>
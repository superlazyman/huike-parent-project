package com.huike.config;

import com.huike.common.config.MinioConfig;
import com.huike.utils.SendEmailUtils;
import com.huike.utils.file.MinioUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;

@Slf4j
@Configuration
public class CommonConfig {

    @Bean
    @ConditionalOnMissingBean
    public MinioUtils minioUtils(MinioConfig minioConfig){
        return new MinioUtils(
                minioConfig.getPort(),
                minioConfig.getEndpoint(),
                minioConfig.getAccessKey(),
                minioConfig.getSecretKey(),
                minioConfig.getBucketName());
    }

    //sendEmaliUtils配置
    @Bean
    public SendEmailUtils sendEmailUtils(JavaMailSender javaMailSender){
        log.info("项目启动加载Email配置类");
        return new SendEmailUtils(javaMailSender);
    }
}

package com.huike.config;

import io.netty.util.concurrent.RejectedExecutionHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.annotation.PreDestroy;
import java.util.concurrent.*;

@Slf4j
@Configuration
@EnableAsync
public class ThreadPoolConfig {

    private ThreadPoolExecutor threadPoolExecutor=null;

    private ThreadPoolTaskExecutor threadPoolTaskExecutor=null;


    @Bean("jdkThreadPool")
    public ThreadPoolExecutor threadPoolExecutor() {
        log.info("初始化项目的线程池（原始）.....");
        threadPoolExecutor= new ThreadPoolExecutor(
                5,
                10,
                5,
                TimeUnit.MINUTES,
                new ArrayBlockingQueue<>(10),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy()
        );
        return threadPoolExecutor;
    }

    @Bean("springThreadPool")
    public ThreadPoolTaskExecutor threadPoolTaskExecutor(){
        log.info("初始化项目的线程池（spring）.....");
        threadPoolTaskExecutor= new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(5);
        threadPoolTaskExecutor.setMaxPoolSize(10);
        threadPoolTaskExecutor.setKeepAliveSeconds(300);
        threadPoolTaskExecutor.setQueueCapacity(10);
        threadPoolTaskExecutor.setThreadNamePrefix("spring-thread-pool");
        return threadPoolTaskExecutor;
    }


    @PreDestroy
    public void destory(){
        if(threadPoolExecutor!=null){
            log.info("项目停止前关闭jdk线程池");
            threadPoolExecutor.shutdown();
        }
        if(threadPoolTaskExecutor!=null){
            log.info("项目停止前关闭spring线程池");
        }
        threadPoolTaskExecutor.shutdown();
    }
}

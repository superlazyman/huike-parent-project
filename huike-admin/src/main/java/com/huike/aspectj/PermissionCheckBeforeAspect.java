package com.huike.aspectj;


import com.huike.common.annotation.PreAuthorize;
import com.huike.common.constant.HttpStatus;
import com.huike.common.exception.CustomException;
import com.huike.domain.system.dto.LoginUser;
import com.huike.web.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Set;

//@Aspect
@Component
@Slf4j
public class PermissionCheckBeforeAspect {
    @Autowired
    private TokenService tokenService;
    @Autowired
    HttpServletRequest httpServletRequest;
    @Autowired
    HttpServletResponse response;

    @Before("execution(* com.huike.controller.*.*.*(..)) && @annotation(preAuthorize)")
    public void authorizeCheck(JoinPoint joinPoint,PreAuthorize preAuthorize){
        LoginUser loginUser = tokenService.getLoginUser(httpServletRequest);
        if(loginUser==null || loginUser.getPermissions()==null){
            response.setStatus(HttpStatus.UNAUTHORIZED);
            throw new CustomException("对不起，令牌过时!",HttpStatus.UNAUTHORIZED);
        }
        Set<String> permissions = loginUser.getPermissions();
        if(permissions.contains("*:*:*")){
            return;
        }
        String permit = preAuthorize.value();
        if(!permissions.contains(permit)){
            response.setStatus(HttpStatus.UNAUTHORIZED);
            throw new CustomException("对不起，没有权限!",HttpStatus.UNAUTHORIZED);
        }
    }
}

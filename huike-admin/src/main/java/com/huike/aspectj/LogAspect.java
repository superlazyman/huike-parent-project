package com.huike.aspectj;


import com.alibaba.fastjson.JSONObject;
import com.huike.common.annotation.Log;
import com.huike.common.exception.CustomException;
import com.huike.domain.system.SysOperLog;
import com.huike.domain.system.dto.LoginUser;
import com.huike.service.ISysOperLogService;
import com.huike.web.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;

@Aspect
@Slf4j
@Component
public class LogAspect {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private ISysOperLogService iSysOperLogService;


    /*@AfterReturning(value = "execution(* com.huike.controller.*.*.*(..)) && @annotation(log)",returning = "result")
    public void after(JoinPoint joinPoint,Log log,Object result){
        handleLog(joinPoint,log,result,null);
    }

    @AfterThrowing(value = "execution(* com.huike.controller.*.*.*(..)) && @annotation(log)",throwing ="throwable")
    public void after(JoinPoint joinPoint,Log log,Throwable throwable){
        handleLog(joinPoint,log,null,throwable);
    }*/

    @Around("execution(* com.huike.controller.*.*.*(..)) && @annotation(log)")
    public Object log(ProceedingJoinPoint proceedingJoinPoint, Log log) throws Throwable {

        Object result;
        try {
            result = proceedingJoinPoint.proceed();
            handleLog(proceedingJoinPoint,log,result,null);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            handleLog(proceedingJoinPoint,log,null,throwable);
            throw new CustomException(throwable.getMessage());
        }
        return result;
    }
    /**
     * 插入日志
     * @param proceedingJoinPoint
     * @param log
     * @param result
     * @param throwable
     */
    private void handleLog(ProceedingJoinPoint proceedingJoinPoint, Log log, Object result, Throwable throwable) {

        SysOperLog sysOperLog=new SysOperLog();
        //1.操作模块
        sysOperLog.setTitle(log.title());
        //2.业务类型
        sysOperLog.setBusinessType(log.businessType().ordinal());
        //3.方法名称
        sysOperLog.setMethod(proceedingJoinPoint.getTarget().getClass().getName()+"."+proceedingJoinPoint.getSignature().getName()+"()");
        //4.请求方式
        sysOperLog.setRequestMethod(request.getMethod());
        //5.操作类别
        String userAgent = request.getHeader("User-Agent");
        Integer type;
        // 判断操作类别
        if (userAgent.contains("Mobile")) {
            // 手机端操作
            type=2;
        } else if(userAgent.contains("Windows")||userAgent.contains("Macintosh")){
            // 电脑端操作
            type=1;
        }else type=0;
        sysOperLog.setOperatorType(type);
        //6.操作人员
        LoginUser loginUser = tokenService.getLoginUser(request);
        if(loginUser!=null) {
            sysOperLog.setOperName(loginUser.getUser().getUserName());
            //7.部门名称
            sysOperLog.setDeptName(loginUser.getUser().getDept()!=null?loginUser.getUser().getDept().getDeptName():null);
            //9.操作地址
            sysOperLog.setOperIp(loginUser.getIpaddr());
            //10.操作地点
            sysOperLog.setOperLocation(loginUser.getLoginLocation());
        }
        //8.请求url
        sysOperLog.setOperUrl(request.getRequestURI());
        //11.请求参数
        sysOperLog.setOperParam(Arrays.toString(proceedingJoinPoint.getArgs()));
        //13.操作状态
        sysOperLog.setStatus(0);
        //返回参数
        sysOperLog.setJsonResult(JSONObject.toJSONString(result));
        //有异常
        if(throwable!=null) {
            //13.操作状态
            sysOperLog.setStatus(1);
            //14.错误信息(有异常)
            sysOperLog.setErrorMsg(throwable.getMessage());
        }
        //15.操作时间
        sysOperLog.setOperTime(new Date());
        //插入日志
        iSysOperLogService.insertOperlog(sysOperLog);
        }
}

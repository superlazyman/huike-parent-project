package com.huike.aspectj;


import com.huike.common.annotation.PreAuthorize;
import com.huike.common.constant.HttpStatus;
import com.huike.common.exception.CustomException;
import com.huike.domain.system.dto.LoginUser;
import com.huike.web.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Set;

@Aspect
@Component
@Slf4j
public class PermissionCheckAspect {
    @Autowired
    private TokenService tokenService;
    @Autowired
    HttpServletRequest httpServletRequest;
    @Autowired
    HttpServletResponse response;

    @Around("execution(* com.huike.controller.*.*.*(..)) && @annotation(preAuthorize)")
    public Object authorizeCheck(ProceedingJoinPoint proceedingJoinPoint, PreAuthorize preAuthorize) throws Throwable {
        //1.获取当前登录用户所有权限标识
        LoginUser loginUser = tokenService.getLoginUser(httpServletRequest);
        if(loginUser==null || loginUser.getPermissions()==null){
            response.setStatus(HttpStatus.UNAUTHORIZED);
            return null;
        }
        //2.获取方法上的注解的权限标识
        String permit = preAuthorize.value();
        //3.判断哟用户是否具有该权限，如果没有，响应401
        Set<String> permissions = loginUser.getPermissions();
        if(!permissions.contains("*:*:*") && !permissions.contains(permit)){
            response.setStatus(HttpStatus.UNAUTHORIZED);
            return null;
        }
        //4.放行
        return proceedingJoinPoint.proceed();
    }
}

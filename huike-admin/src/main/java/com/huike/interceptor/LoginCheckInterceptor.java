package com.huike.interceptor;

import com.huike.common.constant.Constants;
import com.huike.common.constant.HttpStatus;
import com.huike.domain.system.dto.LoginUser;
import com.huike.utils.StringUtils;
import com.huike.web.service.TokenService;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class LoginCheckInterceptor implements HandlerInterceptor {

    @Autowired
    private TokenService tokenService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String jwt = tokenService.getToken(request);
        log.info("请求令牌：{}",jwt);
        if(StringUtils.isEmpty(jwt)){
            log.info("请求中携带的jwt令牌为空，未登录");
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return false;
        }
        try {
            tokenService.parseToken(jwt);
        }catch (Exception e){
            log.info("请求中携带的令牌非法,未登录");
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return false;
        }
        LoginUser loginUser = tokenService.getLoginUser(request);
        if(loginUser==null){
            log.info("用户登录信息已过期");
            response.setStatus(HttpStatus.UNAUTHORIZED);
            return false;
        }
        tokenService.refreshAndCacheToken(loginUser);
        return true;
    }
}

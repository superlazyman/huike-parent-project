package com.huike.task;


import com.huike.domain.system.SysUser;
import com.huike.mapper.SysUserMapper;
import com.huike.mapper.TbBusinessMapper;
import com.huike.mapper.TbClueMapper;
import com.huike.mapper.TbContractMapper;
import com.huike.utils.SendEmailUtils;
import com.huike.utils.file.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;

@Component
public class DataStatisticTask {

    @Autowired
    private TbClueMapper tbClueMapper;
    @Autowired
    private TbBusinessMapper tbBusinessMapper;
    @Autowired
    private TbContractMapper tbContractMapper;
    @Autowired
    private SendEmailUtils sendEmailUtils;
    @Autowired
    private SysUserMapper sysUserMapper;

    @Value("${spring.mail.username}")
    private String from;

    @Scheduled(cron = "0 0/5 * * * ?")
    public void monthDataStaticMail() throws Exception {
        //获取当月开始时间和结束时间
        LocalDate beginDate=LocalDate.of(LocalDate.now().getYear(),LocalDate.now().getMonth(),1);
        LocalDate endDate= YearMonth.now().atEndOfMonth();
        LocalDateTime begin = LocalDateTime.of(beginDate, LocalTime.MIN);
        LocalDateTime end = LocalDateTime.of(endDate,LocalTime.MAX);
        //获取上个月开始时间和结束时间
        LocalDate lastBeginDate=LocalDate.of(LocalDate.now().getYear(),LocalDate.now().getMonth().minus(1),1);
        LocalDate lastEndDate= YearMonth.now().minusMonths(1).atEndOfMonth();
        LocalDateTime lastBegin = LocalDateTime.of(lastBeginDate, LocalTime.MIN);
        LocalDateTime lastEnd = LocalDateTime.of(lastEndDate,LocalTime.MAX);
        //1.查询数据
        //该月新增线索数
        Integer clueCountThisMonth = tbClueMapper.selectCountClueByCreatTime(begin,end);
        //上月新增线索数
        Integer clueCountLastMonth = tbClueMapper.selectCountClueByCreatTime(lastBegin, lastEnd);
        //环比增长
        Integer clueIncreateRate=((clueCountThisMonth-clueCountLastMonth)/clueCountLastMonth)*100;


        //该月新增商机数
        Integer businessCountThisMonth = tbBusinessMapper.selectCountBusinessByCreatTime(begin,end);
        //上月新增商机数
        Integer businessCountLastMonth = tbBusinessMapper.selectCountBusinessByCreatTime(lastBegin,lastEnd);
        //环比增长
        Integer businessIncreateRate=((businessCountThisMonth-businessCountLastMonth)/businessCountLastMonth)*100;


        //该月新增合同数
        Integer concractCountThisMonth = tbContractMapper.selectCountContractByCreatTime(begin,end);
        //上月新增合同数
        Integer concractCountLastMonth = tbContractMapper.selectCountContractByCreatTime(lastBegin,lastEnd);
        //环比增长
        Integer concractIncreateRate=((concractCountThisMonth-concractCountLastMonth)/concractCountLastMonth)*100;


        //该月新增销售额
        Double concractOrderCountThisMonth = tbContractMapper.selectCountcoursePriceByCreatTime(begin,end);
        //上月新增销售额
        Double concractOrderCountLastMonth = tbContractMapper.selectCountcoursePriceByCreatTime(lastBegin,lastEnd);
        //环比增长
        Double concractOrderIncreateRate=((concractOrderCountThisMonth-concractOrderCountLastMonth)/concractOrderCountLastMonth)*100;
        //2.发送邮件
        String text=loadText();
        text = String.format(text, clueCountThisMonth,clueIncreateRate, businessCountThisMonth, businessIncreateRate, concractCountThisMonth, concractIncreateRate,Math.round(concractOrderCountLastMonth),Math.round(concractOrderIncreateRate));
        SysUser sysUser = sysUserMapper.selectUserById(1L);
        sendEmailUtils.SendEMailWithoutAttachement(from,sysUser.getEmail(),"每月数据统计",text,null);

    }

    private String loadText() throws Exception {
        String file = this.getClass().getClassLoader().getResource("template/table.html").getFile();
        String text = FileUtils.readFileToString(new File(file), "UTF-8");
        return text;
    }
}

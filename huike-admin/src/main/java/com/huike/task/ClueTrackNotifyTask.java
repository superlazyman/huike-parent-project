package com.huike.task;

import com.huike.domain.clues.TbClue;
import com.huike.domain.system.SysUser;
import com.huike.mapper.TbAssignRecordMapper;
import com.huike.mapper.TbClueMapper;
import com.huike.utils.SendEmailUtils;
import com.huike.utils.file.FileUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class ClueTrackNotifyTask {

    @Autowired
    private SendEmailUtils sendEmailUtils;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private TbClueMapper tbClueMapper;
    @Autowired
    private TbAssignRecordMapper tbAssignRecordMapper;

    @Value("${spring.mail.username}")
    private String from;

    private static final String CLUE_NOTIFY_KEY = "clue:track:notify:";

    @Scheduled(cron = "0 0/5 * * * ?")
    public void sendNotifyEmail() throws Exception {
        //1.查询需要提醒的线索
        List<TbClue> tbClueList= tbClueMapper.selectCluByNextTime(LocalDateTime.now(),LocalDateTime.now().plusHours(2),"2");
        //2.获取这些线索的归属人，归属人的手机号
        if(!CollectionUtils.isEmpty(tbClueList)){
            for (TbClue tbClue : tbClueList) {
                //3.如果已经提供了，就直接忽略
                if(validateEmail(tbClue.getId())){//redis没有记录
                    //4.查询线索跟进人，发送邮件
                    SysUser sysUser=tbAssignRecordMapper.selectAssignEmailByAssignId(tbClue.getId(),"0");
                    if(sysUser!=null){
                        sendMail(sysUser,tbClue);
                    }
                }
            }
        }
    }

    /**
     * 发送邮件
     * @param sysUser
     * @param tbClue
     */
    private void sendMail(SysUser sysUser, TbClue tbClue) throws Exception {
        String text=loadClueNotifyTemplate();
        text=String.format(text,sysUser.getRealName(),tbClue.getId(),tbClue.getName(),tbClue.getPhone(),sysUser.getSex().equals("0")?"男":"女");
        sendEmailUtils.SendEMailWithoutAttachement(from,sysUser.getEmail(),"线索跟踪提醒",text,null);
        log.info("发送邮件给{}，标题：{}，内容：{}",sysUser.getEmail(),"线索跟踪提醒",text);

        redisTemplate.opsForValue().set(CLUE_NOTIFY_KEY+tbClue.getId(),"1",3, TimeUnit.HOURS);
    }

    /**
     * 加载模板文件
     * @return
     */
    private String loadClueNotifyTemplate() throws Exception {
        String file =this.getClass().getClassLoader().getResource("template/ClueNotifyTemplate.html").getFile();
        String text= FileUtils.readFileToString(new File(file),"UTF-8");
        return text;
    }

    /**
     * 校验邮件是否已经发送，true：未发送，flase：已发送
     * @param id
     * @return
     */
    private boolean validateEmail(Long id) {
        Object o = redisTemplate.opsForValue().get(CLUE_NOTIFY_KEY + id);
        return ObjectUtils.isEmpty(o);
    }
}

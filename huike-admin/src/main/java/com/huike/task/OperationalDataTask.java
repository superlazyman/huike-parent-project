package com.huike.task;


import com.huike.domain.report.vo.DataStatistic;
import com.huike.domain.report.vo.OperationalDataVO;
import com.huike.domain.system.SysUser;
import com.huike.mapper.SysUserMapper;
import com.huike.mapper.TbBusinessMapper;
import com.huike.mapper.TbClueMapper;
import com.huike.mapper.TbContractMapper;
import com.huike.utils.SendEmailUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class OperationalDataTask {
    @Autowired
    private TbClueMapper tbClueMapper;
    @Autowired
    private TbBusinessMapper tbBusinessMapper;
    @Autowired
    private TbContractMapper tbContractMapper;
    @Autowired
    private Configuration configuration;
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private SendEmailUtils sendEmailUtils;
    @Value("${spring.mail.username}")
    private String from;

    /**
     * 每月月底以附件形式发送运营数据
     */
    @Scheduled(cron = "0 0 23 L * ?")
    public void operationalDataSendMailWithAttachment() throws Exception {
        //1.导入模板
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("template/客达天下-月度运营统计.xlsx");
        Workbook workbook=new XSSFWorkbook(in);
        //2.获取时间间隔并插入模板
        LocalDate begin = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());
        LocalDate end = LocalDate.now();
        String timeQuantum=begin+"—————"+end;
        Sheet sheet = workbook.getSheetAt(0);
        sheet.getRow(1).getCell(1).setCellValue(timeQuantum);
        //3.查询总的数据并插入模板
        //新增线索
        Row row3 = sheet.getRow(3);

        LocalDateTime beginTime=LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime=LocalDateTime.of(end, LocalTime.MAX);
        int countAllClues = tbClueMapper.countAllClues(beginTime.toString(),endTime.toString());
        row3.getCell(1).setCellValue(countAllClues);
        //新增商机
        int countAllBusiness = tbBusinessMapper.countAllBusiness(beginTime.toString(), endTime.toString());
        row3.getCell(2).setCellValue(countAllBusiness);
        //新增合同
        Integer countAllContract = tbContractMapper.selectCountContractByCreatTime(beginTime, endTime);
        row3.getCell(3).setCellValue(countAllContract);
        //新增销售额
        Double countAllSales = tbContractMapper.selectCountcoursePriceByCreatTime(beginTime, endTime);
        row3.getCell(4).setCellValue(countAllSales);

        //4.查询每日数据并插入模板
        //4.1获取时间列表
        List<LocalDate> localDateList = begin.datesUntil(end.plusDays(1)).collect(Collectors.toList());
        List<String> dateList = localDateList.stream().map(localDate -> localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
        ).collect(Collectors.toList());
        //4.2获取新增线索列表
        List<DataStatistic> clueDataList=tbClueMapper.countNewCluesByDay(beginTime,endTime);
        Map<String, Integer> clueMap = clueDataList.stream().collect(Collectors.toMap(DataStatistic::getEdate, DataStatistic::getNum));
        //获取全部线索数
//        Integer allClue = clueDataList.stream().map(DataStatistic::getNum).reduce(0, Integer::sum);

        //4.3获取新增商机列表
        List<DataStatistic> businessDataList=tbBusinessMapper.countNewBusinessByDay(beginTime,endTime);
        Map<String, Integer> businessMap = businessDataList.stream().collect(Collectors.toMap(DataStatistic::getEdate, DataStatistic::getNum));

        //4.4获取新增合同列表
        List<DataStatistic> contractAmountDataList=tbContractMapper.countNewContractAmountByDay(beginTime,endTime);
        Map<String, Integer> contractMap = contractAmountDataList.stream().collect(Collectors.toMap(DataStatistic::getEdate, DataStatistic::getNum));
        //4.5新增销售额列表
        Map<String, Double> amountMap = contractAmountDataList.stream().collect(Collectors.toMap(DataStatistic::getEdate, DataStatistic::getAmount));
        //总的销售额
//        contractAmountDataList.stream().map(DataStatistic::getAmount).reduce(0.0,Double::sum);
        List<OperationalDataVO> operationalDataVOList = dateList.stream().map(date ->
                OperationalDataVO.builder()
                        .date(date)
                        .newClue(clueMap.get(date) == null ? 0 : clueMap.get(date))
                        .newBusiness(businessMap.get(date) == null ? 0 : businessMap.get(date))
                        .newContract(contractMap.get(date) == null ? 0 : contractMap.get(date))
                        .sale(amountMap.get(date) == null ? 0 : amountMap.get(date))
                        .build()
        ).collect(Collectors.toList());
        for (int i = 0; i < operationalDataVOList.size(); i++) {
            Row row = sheet.getRow(5 + i);
            row.getCell(0).setCellValue(i+1);
            row.getCell(1).setCellValue(operationalDataVOList.get(i).getDate());
            row.getCell(2).setCellValue(operationalDataVOList.get(i).getNewClue());
            row.getCell(3).setCellValue(operationalDataVOList.get(i).getNewBusiness());
            row.getCell(4).setCellValue(operationalDataVOList.get(i).getNewContract());
            row.getCell(5).setCellValue(operationalDataVOList.get(i).getSale());
        }

        //方式一：以文件方式发送
        /*String filePath="D:\\testData\\月度运行统计报表-明细.xlsx";
        FileOutputStream out=new FileOutputStream(filePath);
        workbook.write(out);
        //5.发送邮件
        SysUser sysUser = sysUserMapper.selectUserById(1L);
        String text="附件是本月运营数据报表，请查收，数据统计方面有任何疑问请联系技术人员确认，谢谢";
        sendEmailUtils.SendEMailWithAttachement(from,sysUser.getEmail(),"月度运行统计报表",text,filePath,null);*/

        //方式二：以字节数组形式发送
        String fileName="月度运行统计报表-明细.xlsx";
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        workbook.write(outputStream);
        //5.发送邮件
        SysUser sysUser = sysUserMapper.selectUserById(1L);
        String text="附件是本月运营数据报表，请查收，数据统计方面有任何疑问请联系技术人员确认，谢谢";
        sendEmailUtils.SendEMailWithAttachement(from,sysUser.getEmail(),"月度运行统计报表",text,fileName,outputStream.toByteArray(),null);
        //6.释放资源
        outputStream.close();
        workbook.close();
    }


    /**
     * 每月月底以表格形式发送运营数据
     */
    @Scheduled(cron = "0 0 23 L * ?")
    public void operationalDataSendMail() throws Exception {
        //1.获取数据
        //1.1获取当前月份日期列表
        LocalDate begin = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());
        LocalDate end = YearMonth.now().atEndOfMonth();
        List<LocalDate> localDateList = begin.datesUntil(end.plusDays(1)).collect(Collectors.toList());
        List<String> dateList = localDateList.stream().map(localDate -> localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
        ).collect(Collectors.toList());
        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);

        //1.2获取新增线索列表
        List<DataStatistic> clueDataList=tbClueMapper.countNewCluesByDay(beginTime,endTime);
        Map<String, Integer> clueMap = clueDataList.stream().collect(Collectors.toMap(DataStatistic::getEdate, DataStatistic::getNum));
//        List<Integer> clueList = dateList.stream().map(date -> clueMap.get(date) == null ? 0 : clueMap.get(date)).collect(Collectors.toList());

        //1.3获取新增商机列表
        List<DataStatistic> businessDataList=tbBusinessMapper.countNewBusinessByDay(beginTime,endTime);
        Map<String, Integer> businessMap = businessDataList.stream().collect(Collectors.toMap(DataStatistic::getEdate, DataStatistic::getNum));
//        List<Integer> businessList = dateList.stream().map(date -> businessMap.get(date) == null ? 0 : businessMap.get(date)).collect(Collectors.toList());

        //1.4获取新增合同列表
        List<DataStatistic> contractAmountDataList=tbContractMapper.countNewContractAmountByDay(beginTime,endTime);
        Map<String, Integer> contractMap = contractAmountDataList.stream().collect(Collectors.toMap(DataStatistic::getEdate, DataStatistic::getNum));
//        List<Integer> contractList = dateList.stream().map(date -> contractMap.get(date) == null ? 0 : contractMap.get(date)).collect(Collectors.toList());
        //1.5新增销售额列表
        Map<String, Double> amountMap = contractAmountDataList.stream().collect(Collectors.toMap(DataStatistic::getEdate, DataStatistic::getAmount));
//        List<Double> amountList = dateList.stream().map(date -> amountMap.get(date) == null ? 0 : amountMap.get(date)).collect(Collectors.toList());

        //2.封装数据到对象中
        /*List<OperationalDataVO> operationalDataVOList=new ArrayList<>();
        for (int i = 0; i < dateList.size(); i++) {
            operationalDataVOList.add(new OperationalDataVO(dateList.get(i),clueList.get(i),businessList.get(i),contractList.get(i),amountList.get(i)));
        }*/
        List<OperationalDataVO> operationalDataVOList = dateList.stream().map(date ->
             OperationalDataVO.builder()
                    .date(date)
                    .newClue(clueMap.get(date) == null ? 0 : clueMap.get(date))
                    .newBusiness(businessMap.get(date) == null ? 0 : businessMap.get(date))
                    .newContract(contractMap.get(date) == null ? 0 : contractMap.get(date))
                    .sale(amountMap.get(date) == null ? 0 : amountMap.get(date))
                    .build()
        ).collect(Collectors.toList());

        //3.获取文本
        //3.1获取模板
        Template template = configuration.getTemplate("operationalTable.ftl");
        //3.2生成文本
        Map<String,Object> dataModer=new HashMap<>();
        dataModer.put("operationalDataVOList",operationalDataVOList);
        String text = FreeMarkerTemplateUtils.processTemplateIntoString(template, dataModer);

        //4.发送邮件
        SysUser sysUser = sysUserMapper.selectUserById(1L);
        sendEmailUtils.SendEMailWithoutAttachement(from,sysUser.getEmail(),"本月运营数据",text,null);
    }

}

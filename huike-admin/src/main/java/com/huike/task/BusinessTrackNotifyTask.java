package com.huike.task;


import com.huike.domain.business.TbBusiness;
import com.huike.domain.system.SysDictData;
import com.huike.domain.system.SysUser;
import com.huike.mapper.TbAssignRecordMapper;
import com.huike.mapper.TbBusinessMapper;
import com.huike.utils.SendEmailUtils;
import com.huike.utils.file.FileUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class BusinessTrackNotifyTask {

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private TbBusinessMapper tbBusinessMapper;
    @Autowired
    private TbAssignRecordMapper tbAssignRecordMapper;
    @Autowired
    private SendEmailUtils sendEmailUtils;
    @Value("${spring.mail.username}")
    private String from;


    private static final String BUSINESS_TRACK_KEY="business:track:notify:";

    @Scheduled(cron = "0 0/5 * * * ?")
    public void businessNotifyTask() throws Exception {
        //1.查询需要提醒的商机
        List<TbBusiness> tbBusinessList= tbBusinessMapper.selectTbBusinessByNextTime(LocalDateTime.now(),LocalDateTime.now().plusHours(2),"2");
        if(!CollectionUtils.isEmpty(tbBusinessList)){
            for (TbBusiness tbBusiness : tbBusinessList) {
                //查看redis看是否已经发过了
                if(validateIfExist(tbBusiness.getId())){
                    //查找商机分配人
                    SysUser sysUser = tbAssignRecordMapper.selectAssignEmailByAssignId(tbBusiness.getId(), "1");
                    if(!ObjectUtils.isEmpty(sysUser)){
                        sendEmail(sysUser,tbBusiness);
                    }
                }

            }

        }
    }

    /**
     * 发送邮件
     * @param sysUser
     * @param tbBusiness
     */
    private void sendEmail(SysUser sysUser, TbBusiness tbBusiness) throws Exception {
        String text=loadEmailText();
        List<SysDictData> sysDictDataList=(List<SysDictData>)redisTemplate.opsForValue().get("sys_dict:course_subject");
        String subject ="";
        for (SysDictData sysDictData : sysDictDataList) {
            if(sysDictData.getDictValue().equals(tbBusiness.getSubject())){
                subject=sysDictData.getDictLabel();
                break;
            }
        }
        text=String.format(text,sysUser.getRealName(),tbBusiness.getId(),tbBusiness.getName(),tbBusiness.getPhone(),subject,tbBusiness.getSex().equals("0")?"男":"女");
        sendEmailUtils.SendEMailWithoutAttachement(from,sysUser.getEmail(),"商机跟进提醒邮件",text,null);
        log.info("商机跟进提醒邮件，发送邮箱：{}，内容：{}",sysUser.getEmail(),text);

        redisTemplate.opsForValue().set(BUSINESS_TRACK_KEY+tbBusiness.getId(),"1",3, TimeUnit.HOURS);
    }

    /**
     * 加载邮件模板
     * @return
     */
    private String loadEmailText() throws Exception {
        String file = this.getClass().getClassLoader().getResource("template/BusinessNotifyTemplate.html").getFile();
        String text = FileUtils.readFileToString(new File(file), "UTF-8");
        return text;
    }

    /**
     * 校验redis中是否已存在该key
     * @param id
     * @return
     */
    private boolean validateIfExist(Long id) {
        Object o = redisTemplate.opsForValue().get(BUSINESS_TRACK_KEY + id);
        return ObjectUtils.isEmpty(o);
    }
}

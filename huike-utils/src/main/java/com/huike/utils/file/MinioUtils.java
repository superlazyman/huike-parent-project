package com.huike.utils.file;

import com.huike.common.config.MinioConfig;
import com.huike.utils.uuid.UUID;
import io.minio.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@AllArgsConstructor
@Data
@Slf4j
public class MinioUtils {

    private int port;
    private String endpoint;
    private String accessKey;
    private String secretKey;
    private String bucketName;


    public String FileUpload(MultipartFile file) throws Exception {
        // Create a minioClient with the MinIO server playground, its access key and secret key.
            MinioClient minioClient =
                    MinioClient.builder()
                            .endpoint(endpoint)
                            .credentials(accessKey, secretKey)
                            .build();

            // Make 'asiatrip' bucket if not exist.
            boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
            if (!found) {
                // Make a new bucket called 'asiatrip'.
                minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
            }

            // Upload '/home/user/Photos/asiaphotos.zip' as object name 'asiaphotos-2015.zip' to bucket
            // 'asiatrip'.
            String originalFilename = file.getOriginalFilename();
            String extName=originalFilename.substring(originalFilename.lastIndexOf("."));
            String fileName= UUID.randomUUID()+extName;
            String objectName=LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/"))+fileName;
//            minioClient.uploadObject(
//                    UploadObjectArgs.builder()
//                            .bucket(bucketName)
//                            .object(fileName)
//                            .filename(file.getName())
//                            .build());
            minioClient.putObject(
                    PutObjectArgs.builder()
                            .bucket(bucketName)
                            .object(objectName)
                            .stream(file.getInputStream(), file.getSize(), -1)
                            .contentType(file.getContentType())
                            .build());
            return "/"+bucketName+"/"+objectName;
    }

}
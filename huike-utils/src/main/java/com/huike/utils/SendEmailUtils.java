package com.huike.utils;

import com.huike.common.constant.MessageConstants;
import com.huike.common.exception.CustomException;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

/**
 * 发送邮件
 */
public class SendEmailUtils {

    private JavaMailSender javaMailSender;

    public SendEmailUtils(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }


    /**
     * 发送邮件不带附件
     */
    public  void SendEMailWithoutAttachement(String from,String to,String subject,String text,String...cc) throws Exception {
        if (StringUtils.isEmpty(from)){
            throw new CustomException(MessageConstants.EMAIL_FROM_NOT_NULL);
        }
        if(StringUtils.isEmpty(to)){
            throw new CustomException(MessageConstants.EMAIL_TO_NOT_NULL);
        }
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
        mimeMessageHelper.setFrom(from);
        mimeMessageHelper.setTo(to);
        if(cc!=null && cc.length>0){
            mimeMessageHelper.setCc(cc); //抄送
        }
        mimeMessageHelper.setSubject(subject);
        mimeMessageHelper.setText(text, true);
        javaMailSender.send(mimeMessage);
    }

    /**
     * 发送邮件带附件，接收文件发送
     */
    public void SendEMailWithAttachement(String from, String to, String subject, String text, String filePath, String... cc) throws Exception {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
        mimeMessageHelper.setFrom(from);
        mimeMessageHelper.setTo(to);
        if(cc!=null && cc.length>0){
            mimeMessageHelper.setCc(cc); //抄送
        }
        mimeMessageHelper.setSubject(subject);

        mimeMessageHelper.setText(text, true);
        File file=new File(filePath);
        mimeMessageHelper.addAttachment(file.getName(),file);
        javaMailSender.send(mimeMessage);
    }



    /**
     * 发送邮件带附件，接收文件字节数组发送
     */
    public void SendEMailWithAttachement(String from,String to,String subject,String text,String fileName, byte[] bytes,String...cc) throws Exception {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
        mimeMessageHelper.setFrom(from);
        mimeMessageHelper.setTo(to);
        if(cc!=null && cc.length>0){
            mimeMessageHelper.setCc(cc); //抄送
        }
        mimeMessageHelper.setSubject(subject);

        mimeMessageHelper.setText(text, true);
        mimeMessageHelper.addAttachment(fileName,new ByteArrayResource(bytes));
        javaMailSender.send(mimeMessage);
    }
}

